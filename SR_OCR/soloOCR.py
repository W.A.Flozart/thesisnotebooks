import boto3
import json

wordlist = []
splitList = []

textract = boto3.client('textract', region_name='eu-west-1', aws_access_key_id=f'{access_key}', 
    aws_secret_access_key=f'{secret_key}')

with open(f'{image_path}', 'rb') as document:
    img = bytearray(document.read())

# Call Amazon Textract
try:
    response = textract.detect_document_text(
        Document={'Bytes': img}
    )
    # Print detected text
    for item in response["Blocks"]:
        if item["BlockType"] == "LINE":
            wordlist.append(item["Text"])
except:
    print(f"Problem")

for string in wordlist:
    splitList = splitList + string.split()

json_string = json.dumps(splitList)

print(json_string)

